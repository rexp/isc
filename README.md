ISC
===

Data
----

All data files are listed below.

Downloads from [Box.com](https://www.box.com/):

* [Listing of all files](https://www.box.com/s/c17uhob1fn4xqczc55gv)
* Combined data, sorted [rexp-cd.zip](https://app.box.com/s/l8ty98cohzo5nlhwlfzd)
* Raw merged data, unsorted [rexp-md.zip](https://app.box.com/s/j95v3r9g46v4n2zu1v41)
* Cache from crawling [rexp-hc.tar.gz](https://app.box.com/s/gbemkrccnm40gkd7sx4d)
* Cache from analysis [rexp-an.tar.gz](https://app.box.com/s/kpnvb77ra0gw4ao6h6k2)
